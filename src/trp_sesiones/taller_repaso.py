#Taller de repaso

#1. Hola mundo
print("Hola, mundo!")


#2. Variables y asignación
varia_texto= "Texto"
variable=0
variable=+1


#3. Tipos de datos básicos: int, float, string
numentero = 5
flotante = 5,76
string = "Hola"

print(numentero)
print(flotante)
print (string)


#4  Operaciones aritméticas básicas
numero1 = 5
numero2= 7
numero3= 2
print("numero 1:" , numero1)
print("numero 2:" , numero2)
print("numero 3:" , numero3)

suma= numero1+numero2
print("suma numero 1 y numero 2:" ,  suma)

resta= numero2-numero3
print("resta numero 2 menos numero 3:" , resta)

multiplicacion= numero1 * numero3
print("multiplicación del numero 1 y numero 3:" , multiplicacion)

division = numero2/numero3
print ("division del numero2 y numero3" , division)


#5 Conversión entre tipos de datos
numero_texto= "42"
numero_int = int(numero_texto)
print (numero_int)

numero = 53
texto = str(numero)
print (texto)


#6 Solicitar entrada del usuario
nombre = input("Ingresa tu nombre: ")
print("Hola,", nombre)


#7 Condicional if
t="Hola"
t2="Adios"

if t==t2:
    print("Son el mismo texto")


#8 Condicional if-else.
n=5
n2=13

print("n=" , n)
print("n2=", n2)

if n < n2:
    print("n2 es mayor que n")
else:
    print("n es mayor que n2")


#9 Condicional if-elif-else

if numero1 == numero2:
    print("Los números son iguales")
elif numero1 > numero2:
    print("numero 1 es mayor que numero2 ")
else:
    print("numero2 es mayor que numero 1")


#10 Bucle for
numeros = [1, 2, 3, 4, 5]
print ("Bucle for")
#Recorre lista de números
for numero in numeros:
    print(numero)
print("________________")


#11 Bucle while
contador = 1
print ("Bucle while")

while contador <= 5:
    #Imprime números del 1 al 5
    print(contador) 
    contador += 1  

print("________________")


#12 Uso de break y continue.
print ("Bucle for con break")
for num in range(0, 9):
    print(num)

    if num == 6:
        break

print("________________")
print ("Bucle while con continue")
contador = 1
while contador <= 5:
    if contador == 3:
        contador += 1 
        continue 
    print(contador)
    contador += 1


#13 Listas y sus operaciones básicas
lista = [2, 4, 6, 8, 10]
print("Lista:",lista)

primer_elemento = lista[0]  # Acceder a un elemento de la lista 
print("Primer elemento de la lista:",primer_elemento)

longitud = len(lista)  #Cantidad de elementos en la lista
print("Cantidad de elementos de la lista:", longitud)

lista.append(12)  #Agregar elemento a la lista
lista.remove(2)   #Eliminar elemento
lista[4] = 7   #Cambiar elemento

lista2 = lista.copy() #Copia de la lista


#14 Tuplas y su inmutabilidad
tupla = (1, 2, 3)
print("Tupla", tupla)
tupla2 = (5.7, 1, "texto") #tupla con diferentes tipos de datos


#15 Conjuntos y operaciones
conjunto1 = {1, 2, 3}
conjunto2 = {4, 5, 6}

conjunto1.add(4) #Agregar elemento
conjunto2.add(7)

print("Conjunto1:",conjunto1)
print("Conjunto2:",conjunto2)

union = conjunto1.union(conjunto2) 
print ("Union de los conjuntos:",union)
interseccion = conjunto1.intersection(conjunto2) 
print("Intersección de los conjuntos:", interseccion)
diferencia=conjunto1.difference(conjunto2)
print("Diferencia conjunto1 y conjunto2", diferencia)

conjunto2.remove(5)


#16 Diccionarios y operaciones clave-valor
diccionario = {
     "Nombre": "Maria",
     "Direccion": "Calle e3",
     "Edad": 19}
print ("Diccionario", diccionario)

print(diccionario["Nombre"]) #Imprimir solo un dato específico
diccionario["Direccion"] = "Calle 45E#2"  # Cambiar dato
print ("Diccionario", diccionario)


 #17 List funciones básicas
lista = [10, 20, 30, 40]
print("List",lista)
print(len(lista)) #Tamaño 


#18 Leer un archivo de texto.
archivo = open('archivo.txt', 'r')
contenido = archivo.read()
archivo.close()
print(contenido)


#19 Escribir en un archivo de texto
with open('nombre_del_archivo.txt', 'w') as archivo:
    archivo.write("texto para el archivo\n")
    
    
#20 Modos de apertura: r, w, a, rb, wb
with open('archivo.txt', 'r') as archivo:
    archivo.read()
    
    with open('archivo.txt', 'w') as archivo:
        archivo.write ("texto")
        
        with open('archivo.txt', 'a') as archivo:
            archivo.write("texto")
              
        with open('archivo.bin', 'rb') as archivo_binario:
         archivo.read()
         
         with open('archivo.bin', 'wb') as archivo_binario:
              archivo.write (" ")


#21 Trabajar con archivos JSON
import json

# Datos en forma de diccionario de Python
datos = {
    "nombre": "María",
    "edad": 25,
    "ciudad": "Medellín",
    "intereses": ["fotografía", "música"]
}
#Escribir datos en JSON
with open('datos.json', 'w') as archivo_json:
    json.dump(datos, archivo_json)



#Crear un dataframe
import pandas as pd

informacion = {
    "producto": ["camisa", "zapatos", "vestido", "pantalon"],
    "precio": [100, 305, 143, 275],
    "color": ["verde", "azul", "gris", "azul"]
  
}
df = pd.DataFrame(informacion)


#22 Leer un archivo CSV.
df = pd.read_csv('texto.csv')
print(df)


# 23. Filtrar datos en un DataFrame.
precios_mayores = df[df['precio'] > 200]
print(precios_mayores)


#24 Operaciones básicas: sum(), mean(), max(), min()
suma_precios = df.sum()
print(suma_precios)

promedio_total = df.mean()
print(promedio_total)

maxima_cantidad = df.max()
print(maxima_cantidad)

minimo_cantidad = df.min()
print(minimo_cantidad)


#25 Uso de iloc y loc
fila = df.iloc[0] 
filas = df.iloc[0:2]  

fila = df.loc['producto']


#26 Agrupar datos con groupby
grouped_df = df.groupby('color')
print(grouped_df.get_group('azul'))


#27 Unir DataFrames con merge y concat

informacion2 = {
    "producto": ["Falda", "sudadera", "tacones", "medias"],
    "precio": [129, 415, 531, 231],
    "color": ["morado", "negro", "rojo", "azul"]
}
df2 = pd.DataFrame(informacion2)

pd.concat([df, df2], axis=1)
df.merge(df2, on="precio", how="left")


#28 Manipular series temporales
import datetime
fechas = [datetime.date(2023, 1, 1), datetime.date(2023, 1, 2), datetime.date(2023, 1, 3)]
valores = [100, 150, 200]

serie_temporal = pd.Series(valores, index=fechas)


#29 Exportar un DataFrame a CSV
df.to_csv("src\trp_sesiones\texto\texto.csv")

#30 Convertir un DataFrame a JSON
df.to_json('archivo.json')




