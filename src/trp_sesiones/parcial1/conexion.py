import sqlite3

class Conexion:
    def __init__(self, db_hotel):
        self.connection = sqlite3.connect(db_hotel)
        self.cursor = self.connection.cursor()

def crear_tabla_clientes(self):
        
    sql_crear_tabla_clientes = """
    CREATE TABLE IF NOT EXISTS clientes (
        idCliente INTEGER PRIMARY KEY,
        nombre TEXT,
        apellido TEXT,
        dni TEXT,
        telefono TEXT,
        direccion TEXT,
        correoElectronico TEXT
    )
    """
    self.cursor.execute(sql_crear_tabla_clientes)  
    self.connection.commit()

def crear_tabla_reserva(self):
        
    sql_crear_tabla_reserva = """
    CREATE TABLE IF NOT EXISTS reservas (
    idReserva INTEGER PRIMARY KEY,
    fechaEntrada DATETIME,
    fechaSalida DATETIME,
    cliente_id INTEGER,
    hotel TEXT,
    total REAL,
    FOREIGN KEY (cliente_id) REFERENCES clientes (idCliente)
    )
    """
    self.cursor.execute(sql_crear_tabla_reserva)  
    self.connection.commit()


def crear_tabla_habitacion(self):
        
    sql_crear_tabla_habitacion = """
     CREATE TABLE IF NOT EXISTS habitaciones (
    idHabitacion INTEGER PRIMARY KEY,
    numero INTEGER,
    tipo TEXT,
    capacidad INTEGER,
    precioNoche REAL,
    amenities TEXT,
    vista TEXT
    )
    """
    self.cursor.execute(sql_crear_tabla_habitacion) 
    self.connection.commit()
    
      
       

    def close(self):
        self.connection.close()
        
        
        
  