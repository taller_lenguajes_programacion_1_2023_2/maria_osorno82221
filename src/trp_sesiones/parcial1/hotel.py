class Hotel:
    def __init__(self, idHotel, numero, direccion, estrellas, servicios, numeroHabitaciones, dueño):
        self.__idHotel = idHotel
        self.__numero = numero
        self.__direccion = direccion
        self.__estrellas = estrellas
        self.__servicios = servicios
        self.__numeroHabitaciones = numeroHabitaciones
        self.__dueño = dueño

   
    def get_idHotel(self):
        return self.__idHotel


    def set_idHotel(self, idHotel):
        self.__idHotel = idHotel


    def get_numero(self):
        return self.__numero


    def set_numero(self, numero):
        self.__numero = numero

 
    def get_direccion(self):
        return self.__direccion


    def set_direccion(self, direccion):
        self.__direccion = direccion

   
    def get_estrellas(self):
        return self.__estrellas


    def set_estrellas(self, estrellas):
        self.__estrellas = estrellas


    def get_servicios(self):
        return self.__servicios


    def set_servicios(self, servicios):
        self.__servicios = servicios

 
    def get_numeroHabitaciones(self):
        return self.__numeroHabitaciones

  
    def set_numeroHabitaciones(self, numeroHabitaciones):
        self.__numeroHabitaciones = numeroHabitaciones

   
    def get_dueño(self):
        return self.__dueño

   
    def set_dueño(self, dueño):
        self.__dueño = dueño
