from cliente import Cliente
from hotel import Hotel


class Habitacion:
    def __init__(self, idHabitacion, numero, tipo, capacidad, precioNoche, amenities, vista):
        self.__idHabitacion = idHabitacion
        self.__numero = numero
        self.__tipo = tipo
        self.__capacidad = capacidad
        self.__precioNoche = precioNoche
        self.__amenities = amenities
        self.__vista = vista

 
    def get_idHabitacion(self):
        return self.__idHabitacion

    def set_idHabitacion(self, idHabitacion):
        self.__idHabitacion = idHabitacion
        
    def get_numero(self):
        return self.__numero


    def set_numero(self, numero):
        self.__numero = numero

  
    def get_tipo(self):
        return self.__tipo

  
    def set_tipo(self, tipo):
        self.__tipo = tipo

    
    def get_capacidad(self):
        return self.__capacidad

    
    def set_capacidad(self, capacidad):
        self.__capacidad = capacidad

    def get_precioNoche(self):
        return self.__precioNoche

  
    def set_precioNoche(self, precioNoche):
        self.__precioNoche = precioNoche

  
    def get_amenities(self):
        return self.__amenities

  
    def set_amenities(self, amenities):
        self.__amenities = amenities

  
    def get_vista(self):
        return self.__vista

 
    def set_vista(self, vista):
        self.__vista = vista    




class Reserva(Habitacion) :
    
    def __init__(self, idReserva, fechaEntrada, fechaSalida, cliente, hotel, total):
        self.__idReserva = idReserva
        self.__fechaEntrada = fechaEntrada
        self.__fechaSalida = fechaSalida
        self.__cliente = Cliente()
        self.__hotel = Hotel()
        self.__total = total
       
        
        super().__init__()

   
    def get_idReserva(self):
        return self.__idReserva

  
    def set_idReserva(self, idReserva):
        self.__idReserva = idReserva

   
    def get_fechaEntrada(self):
        return self.__fechaEntrada

   
    def set_fechaEntrada(self, fechaEntrada):
        self.__fechaEntrada = fechaEntrada

 
    def get_fechaSalida(self):
        return self.__fechaSalida

   
    def set_fechaSalida(self, fechaSalida):
        self.__fechaSalida = fechaSalida
        
        
    def set_cliente(self, cliente):
        self.__cliente =cliente

   
    def get_cliente(self):
        return self.__cliente
    
    
    def set_hotel(self,hotel):
        self.__hotel =hotel

   
    def get_total(self):
        return self.__total

   
    def set_total(self, total):
        self.__total = total
        
        
    

   
    def get_hotel(self):
        return self.__hotel
    

    def __str__(self):
        return f"ID de Reserva: {self.__idReserva}\n" \
               f"Fecha de Entrada: {self.__fechaEntrada}\n" \
               f"Fecha de Salida: {self.__fechaSalida}\n" \
               f"Cliente: {self.__cliente}\n" \
               f"Hotel: {self.__hotel}\n" \
               f"Total: {self.__total}"
    
    