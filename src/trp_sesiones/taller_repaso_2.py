#Taller 2
import pandas as pd

#1 Lee una tabla HTML desde una página y guárdalo en un DataFrame. 
tabla=pd.read_html("https://www.paises.net/america/#:~:text=Existen%20actualmente%2035%20pa%C3%ADses%20en%20Ame%CC%81rica%2C%20reconocidos%20por,encuentra.%20Lista%20de%20todos%20los%20pa%C3%ADses%20de%20Ame%CC%81rica")[0]
df_html=pd.DataFrame(tabla)
print(df_html)


#2 guardar la tabla de html en un excel
df_html.to_excel("./src/trp_sesiones/xlsx/archivo.xlsx")


#3 Exporta los primeros 10 registros del DataFrame anterior a un archivo CSV
primeros_10 = df_html.head(10)
primeros_10.to_csv('./src/trp_sesiones/texto/primeros_10_registros.csv', index=False)


#4 Lee tres tablas HTML y guárdalas en tres hojas diferentes de un archivo Excel "tres_tablas.xlsx"
Hoja1=pd.read_html("https://www.paises.net/oceania/") [0]
Hoja2=pd.read_html("https://www.paises.net/europa/") [0]
Hoja3=pd.read_html("https://www.paises.net/asia/") [0]

with pd.ExcelWriter("./src/trp_sesiones/xlsx/tres_tablas.xlsx") as writer:
    Hoja1.to_excel(writer,sheet_name="Hoja1")
    Hoja2.to_excel(writer,sheet_name="Hoja2")
    Hoja3.to_excel(writer,sheet_name="Hoja3")


#5 Lee un archivo Excel, filtra los registros donde la columna "edad" sea mayor a 30 y guárdalos en "mayores30.csv"
archivo_excel=pd.read_excel("./src/trp_sesiones/xlsx/clase_4_dataset.xlsx")
df_edades=pd.DataFrame(archivo_excel)
mayores_a_30 = archivo_excel[archivo_excel["Edad"] > 30]
print(mayores_a_30)
mayores_a_30.to_csv('./src/trp_sesiones/texto/mayores_a_30.csv', index=False)


#6 Lee un archivo CSV, selecciona solo dos columnas y guárdalas en un archivo Excel
archivo_csv=pd.read_csv('./src/trp_sesiones/texto/mayores_a_30.csv')
columnas = ['Nombre', 'Teléfono']
archivo_csv[columnas].to_excel("./src/trp_sesiones/xlsx/nombres_telefonos.xlsx")


#7 Lee la primera y tercera hoja del archivo Excel y combínalas en un DataFrame
xlsx = pd.ExcelFile("./src/trp_sesiones/xlsx/tres_tablas.xlsx")
df_hoja1 = pd.read_excel(xlsx, 'Hoja1')
df_hoja2 = pd.read_excel(xlsx, 'Hoja3')
combinacion=pd.concat([df_hoja1,df_hoja2])
combinacion.to_excel("./src/trp_sesiones/xlsx/varias_hojas.xlsx")