from .conexion import Conexion
from tkinter import messagebox

class Plato(Conexion):
    """Clase que hereda de Conexion y se usa para los platos
    Tiene los métodos guardar, editar, eliminar y listar con querys en sql para
    manipular la base de datos  
    """
    
    def __init__(self,nombre_plato="", chef_plato="") :
        super().__init__()
        self.id_plato = None
        self.nombre_plato = nombre_plato
        self.chef_plato = chef_plato
      
        self.conexion = Conexion() #Clase conexion
        self.cursor = self.conexion.cursor
        
        
    def guardar(self):
        query = """
        INSERT INTO platos ( nombre_plato, chef_plato)
        VALUES ('{}' , '{}' , '{}')
        """.format(self.nombre_plato,self.chef_plato)
        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)

    def editar(self,id_plato=""):
        query = """
        UPDATE platos
        SET nombre_plato = '{}' , chef_plato= '{}'
        WHERE id_chef = {}
        """.format(self.nombre_plato,self.chef_plato,id_plato)

        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)

    def eliminar(self,id_plato=""):
        if id_plato != "":
            query = """
            DELETE FROM platos WHERE id_plato = '{}'
            """.format(id_plato)
            
            try:
                self.cursor.execute(query)
                self.conexion.cerrar()
                messagebox.showinfo(" Se elimino correctamente el registro {}".format(id_plato))
            except Exception as error:
                print(error)
                messagebox.showerror("No se pudo eliminar el registro")
        else:
            print("error: no hay id para eliminar")

    def listar(self):
        self.conexion = Conexion()
        query = """
        SELECT * FROM platos
        """
        lista= []
        try:
            self.cursor.execute(query)
            lista = self.cursor.fetchall()
            self.conexion.cerrar()
        except Exception as error:
            print(error)
        return lista 

  
        
    
    
    def __str__(self) -> str:
        return "platos {} , {}".format(self.nombre_plato,self.chef_plato) 
