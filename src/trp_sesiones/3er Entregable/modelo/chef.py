from .conexion import Conexion
from tkinter import messagebox

class Chef(Conexion):
    """Clase que hereda de Conexion y se usa para los Chefs
    Tiene los métodos guardar, editar, eliminar y listar con querys en sql para
    manipular la base de datos 
    """
    def __init__(self,nombre_chef="",especialidad_chef="", id_chef="") :
        super().__init__()
        self.id_chef = id_chef
        self.nombre_chef = nombre_chef
        self.especialidad_chef = especialidad_chef
      
        self.conexion = Conexion()
        self.cursor = self.conexion.cursor
        
        
    def guardar(self):
        query = """
        INSERT INTO chefs (id_chef, nombre_chef, especialidad_chef)
        VALUES ('{}', '{}' , '{}')
        """.format(self.id_chef, self.nombre_chef,self.especialidad_chef)
        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)

    def editar(self,id_chef=""):
        query = """
        UPDATE chefs 
        SET nombre_chef = '{}' , especialidad_chef = '{}'
        WHERE id_chef = {}
        """.format(self.nombre_chef,self.especialidad_chef,id_chef)

        try:
            self.cursor.execute(query)
            self.conexion.cerrar()
        except Exception as error:
            print(error)

    def eliminar(self,id_chef=""):
        if id_chef != "":
            query = """
            DELETE FROM chefs WHERE id_chef = '{}'
            """.format(id_chef)
            try:
                self.cursor.execute(query)
                self.conexion.cerrar()
                messagebox.showinfo(" Se elimino correctamente el registro {}".format(id_chef))
            except Exception as error:
                print(error)
                messagebox.showerror("No se pudo eliminar el registro")
        else:
            print("error: no hay id para eliminar")

    def listar(self):
        self.conexion = Conexion()
        query = """
        SELECT * FROM chefs
        """
        lista= []
        try:
            self.cursor.execute(query)
            lista = self.cursor.fetchall()
            self.conexion.cerrar()
        except Exception as error:
            print(error)
        return lista 

  
        
    
    
    def __str__(self) -> str:
        return "chef {} , {}".format(self.nombre_chef,self.especialidad_chef)
