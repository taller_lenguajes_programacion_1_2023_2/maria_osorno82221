import sqlite3
from tkinter import messagebox

class Conexion:
    """Clase que se conecta con la base de datos y es la encargada de manejar y crear las tablas 
    para los chefs y los platos en esta
    """
    
    def __init__(self):
        self.ruta_db = 'C:/Users/Maria Ximena/Documents/Nueva carpeta/maria_osorno82221/src/trp_sesiones/3er Entregable/base de datos/BaseDatos.sqlite'
        
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()
        self.crear_tabla_chef()
        #self.crear_tabla_platos()
        
    
    def crear_tabla_chef(self):
        query= """
        CREATE TABLE IF NOT EXISTS chefs (
            id_chef INTEGER PRIMARY KEY,
            nombre_chef VARCHAR (50), 
            especialidad_chef VARCHAR (50)
        )"""
        

        try:
            
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("Info", "Se creó la tabla")
        except Exception as error:
                print(error)
                messagebox.showerror("Error", "No se pudo crear la tabla")
                
                
    def crear_tabla_platos(self):
        query= """
        CREATE TABLE IF NOT EXISTS platos (
            id_plato INTEGER PRIMARY KEY AUTOINCREMENT,
            nombre_plato VARCHAR (50), 
            chef_plato VARCHAR (50)
        )"""
        

        try:
            
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("Info", "Se creó la tabla")
        except Exception as error:
                print(error)
                messagebox.showerror("Error", "No se pudo crear la tabla")            

    def borrar_tabla(self):
        query= """
        DROP TABLE IF EXISTS chefs"""
        try:
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("INFO : Borrado Tabla Exitosa","Se eliminó la tabla correctamente")
        except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Borrado Tabla No Exitosa","No se pudo eliminar la tabla")
   
    
    def cerrar(self):
        self.conexion.commit()
        self.conexion.close()
