import tkinter as tk
from tkinter import ttk , messagebox
from tkinter import messagebox
from tkinter import PhotoImage
from PIL import Image, ImageTk
from modelo.chef import Chef
from modelo.plato import Plato

class Frame (tk.Frame):   
     """
     Esta clase contiene la interfaz princiapal, 
     desde la cual se llaman las otras dos interfaces
     
     Tiene el método botones, que crea los botones que llevarán a la otra interfaz
     Y el método imagen para poner una imagen en la interfaz
    """
     def __init__(self, root=None):
        super().__init__(root, width=480, height=320, bg='lightblue')
        self.root = root
        self.pack()
        self.root.title("Chefs y Platos")
        self.botones()
        #self.imagen()
        
     def botones (self):
         
        self.btn_volver = tk.Button(self, text='Ver Chefs', command=self.interfaz_chefs)
        self.btn_volver.config(width=20, font=('Arial', 12, 'bold'), bg='#FFE1BD')
        self.btn_volver.grid(row=1, column=0, padx=10, pady=10)  
        
        self.btn_volver = tk.Button(self, text='Ver Platos', command=self.interfaz_platos)
        self.btn_volver.config(width=20, font=('Arial', 12, 'bold'), bg='#FFE1BD')
        self.btn_volver.grid(row=1, column=2, padx=10, pady=10)    
        
     def imagen (self, root):
         
       path_to_image = "C:/Users/Maria Ximena/Documents/Nueva carpeta/maria_osorno82221/src/trp_sesiones/3er Entregable/firma/chef.jpg"
       img = PhotoImage(file=path_to_image)

        # Mostrar la imagen en un Label
       label = tk.Label(root, image=img)
       label.image = img 
       label.grid()
    
        
        
     def interfaz_chefs (self):
        self.root.destroy()  # Cierra la ventana actual
        self.root = tk.Tk() 
        interfaz_chef= Interfaz_Chef(self.root)
        interfaz_chef.__init__
        
     def interfaz_platos (self):
        self.root.destroy()  # Cierra la ventana actual
        self.root = tk.Tk() 
        interfaz_plato= Interfaz_Plato(self.root)
        interfaz_plato.__init__   
        
    

class Interfaz_Chef(tk.Frame):
    """Crea la interfaz para Chefs
    Tiene los métodos:
    campos_chef que crea las etiquetas, los campos de texto y botones para la información del chef
    habilitar_campos que habilita la escritura en los campos de texto
    desabilitar_campos que deshabilita la escritura en los campos de texto
    guardar_datos que lleva los datos a otra clase que los guardará en bd, se llega a esta con el botón guardar
    tabla_chef que crea la tabla para mostrar los datos de los chefs
    mostrar_datos que trae los datos de la bd a la tabla
    editar_datos que permite editar los datos de la tabla
    actualizar_datos que se llama en el método anterior y servirá para actualizar los datos en la bd
    eliminar_datos que sirve para borrar los datos de la bd
    primera_interfaz que llevará a la primera interfaz
    """
    
    
    def __init__(self, root=None):
        super().__init__(root, width=480, height=320, bg='lightblue')
        self.root = root
        self.pack()
        self.root.title("Chefs")
        self.id_chef=None
        self.chef=Chef()
        self.campos_chef()
        self.tabla_chef()
        


    def campos_chef(self):
        
        #Etiquetas
        self.lbl_nombre = tk.Label(self, text='Nombre Chef:' , bg='lightblue')
        self.lbl_nombre.config(font=('Arial', 12, 'bold'))
        self.lbl_nombre.grid(row=0, column=0, padx=10, pady=10)

        self.lbl_especialidad = tk.Label(self, text='Especialidad:' , bg='lightblue')
        self.lbl_especialidad.config(font=('Arial', 12, 'bold'))
        self.lbl_especialidad.grid(row=1, column=0, padx=10, pady=10)
 
        self.lbl_identificacion = tk.Label(self, text='Identificación:' , bg='lightblue')
        self.lbl_identificacion.config(font=('Arial', 12, 'bold'))
        self.lbl_identificacion.grid(row=2, column=0, padx=10, pady=10)  

        #Campos de texto
        self.var_nombre = tk.StringVar()
        self.input_nombre = tk.Entry(self, textvariable=self.var_nombre)
        self.input_nombre.config(width=50, font=('Arial', 12))
        self.input_nombre.grid(row=0, column=1, padx=10, pady=10, columnspan=2)

        self.var_especialidad = tk.StringVar()
        self.input_especialidad = tk.Entry(self, textvariable=self.var_especialidad)
        self.input_especialidad.config(width=50, font=('Arial', 12))
        self.input_especialidad.grid(row=1, column=1, padx=10, pady=10, columnspan=2)
        
        self.var_identificacion = tk.StringVar()
        self.input_identificacion = tk.Entry(self, textvariable=self.var_identificacion)
        self.input_identificacion.config(width=50, font=('Arial', 12))
        self.input_identificacion.grid(row=2, column=1, padx=10, pady=10, columnspan=2)


  
        #Botones 
        self.btn_nuevo = tk.Button(self, text='Nuevo', command=self.habilitar_campos)
        self.btn_nuevo.config(width=20, font=('Arial', 12, 'bold'), bg='#FFFDC9')
        self.btn_nuevo.grid(row=3, column=0, padx=10, pady=10)

        self.btn_guardar = tk.Button(self, text='Guardar', command=self.guardar_datos_chef)
        self.btn_guardar.config(width=20, font=('Arial', 12, 'bold'), bg='#C7FFC6')
        self.btn_guardar.grid(row=3, column=1, padx=10, pady=10)

        self.btn_cancelar = tk.Button(self, text='Cancelar', command=self.desabilitar_campos)
        self.btn_cancelar.config(width=20, font=('Arial', 12, 'bold'), bg='#FFC6C6')
        self.btn_cancelar.grid(row=3, column=2, padx=10, pady=10)
        
        self.btn_volver = tk.Button(self, text='Volver', command=self.primera_interfaz)
        self.btn_volver.config(width=20, font=('Arial', 12, 'bold'), bg='#FFE1BD')
        self.btn_volver.grid(row=9, column=1, padx=10, pady=10)



    def habilitar_campos(self):
        self.var_nombre.set('')
        self.var_especialidad.set('')
        self.var_identificacion.set('')

        self.input_nombre.config(state='normal')
        self.input_especialidad.config(state='normal')
        self.input_identificacion.config(state='normal')

        self.btn_guardar.config(state='normal')
        self.btn_cancelar.config(state='normal')

    def desabilitar_campos(self):
        self.var_nombre.set('')
        self.var_especialidad.set('')
        self.var_identificacion.set('')

        self.input_nombre.config(state='disabled')
        self.input_especialidad.config(state='disabled')
        self.input_identificacion.config(state='disabled')

        self.btn_guardar.config(state='disabled')
        self.btn_cancelar.config(state='disabled')



    
    def guardar_datos_chef(self):
     self.chef = Chef(
        self.var_nombre.get(),
        self.var_especialidad.get(),
        self.var_identificacion.get())
     
     #print(self.chef)
     
     self.chef.guardar() 
     
   
     self.tabla_chef()
     self.desabilitar_campos()

     
    def tabla_chef(self):
         self.lista_chefs = self.chef.listar()
         self.lista_chefs.reverse()
        
         self.tabla = ttk.Treeview(self,
         columns=('Nombre', 'Especialidad'))
         self.tabla.grid(row=5,column=0, columnspan=4,sticky='nse')
        
         self.scroll = ttk.Scrollbar(self,orient='vertical',command=self.tabla.yview)
         self.scroll.grid(row=5,column=0, columnspan=4,sticky='nse')
        
         self.tabla.configure(yscrollcommand=self.scroll.set)
         self.tabla.heading('#0',text='Identificacion')
         self.tabla.heading('#1',text='Nombre del chef')
         self.tabla.heading('#2',text='especialidad')
        
        
         self.boton_editar = tk.Button(self,text="Editar",command=self.editar_datos)
         self.boton_editar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
         self.boton_editar.grid(row=6,column=0,padx=10,pady=10)
        
         self.boton_eliminar = tk.Button(self,text="Eliminar",command=self.eliminar_datos)
         self.boton_eliminar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
         self.boton_eliminar.grid(row=6,column=1,padx=10,pady=10)
         
         
    def mostrar_datos(self):
        
        chefs = Chef() 
        rows = chefs.listar()  # Llamar al método listar de la clase Chef
        for row in rows:
            self.tabla.insert('', 'end', text=row[0], values=(row[1], row[2]))


            
    def editar_datos(self):
        try:
            selected_item = self.tabla.selection()
            
            values = self.tabla.item(selected_item, 'values')
           
            if selected_item:
               values = self.tabla.item(selected_item, 'values')
               
               if values:
                    self.id_chef = values[0]
                    self.nombre_chef = values[1]
                    self.especialidad_chef = values[2]
                    self.habilitar_campos()
                    self.var_identificacion.set(self.id_chef)
                    self.var_nombre.set(self.nombre_chef)
                    self.var_especialidad.set(self.especialidad_chef)
                    self.actualizar_datos()
                    
               else:
                messagebox.showerror("Error", "No se encontraron valores para la selección de la tabla.")
       
            else:
                messagebox.showerror("Error", "No se ha seleccionado ningún elemento en la tabla.")
       
           
        except Exception as error:
            print (error)  
            messagebox.showerror("Error", "Error al editar los datos")
             
     
    def actualizar_datos(self):
        try:
             chef = Chef()
             chef.id_chef = self.var_identificacion.get()
             chef.nombre_chef = self.var_nombre.get()
             chef.especialidad_chef = self.var_especialidad.get()  
             chef.editar()  # Llama método de la clase Chef que actualiza los datos en la base de datos
        
            
        except Exception as error:
            print(error)
            messagebox.showerror("Error", "Error al actualizar los datos en la base de datos")
    
            
                    
    
    def eliminar_datos(self):
        try:
            self.id_chef = self.tabla.item(self.tabla.selection())['text']
            self.chef.eliminar(self.id_chef)# Llama método de la clase Chef que elimina los datos en la base de datos
            self.tabla_chef()
            self.id_chef=None
            
        except Exception as error:
            print(error) 
            messagebox.showerror("Error", "Error al eliminar los datos")
  
    
    def primera_interfaz (self):
        self.root.destroy()  #Cierra la ventana actual para llamar a la otra
        self.root = tk.Tk() 
        frame= Frame(self.root)
        frame.__init__
    
    
    
class Interfaz_Plato(tk.Frame):
    """Crea la interfaz para los platos
    Tiene los métodos:
    campos_plato que crea las etiquetas, los campos de texto y botones para la información de los platos
    habilitar_campos que habilita la escritura en los campos de texto
    desabilitar_campos que deshabilita la escritura en los campos de texto
    guardar_datos que lleva los datos a otra clase que los guardará en bd, se llega a esta con el botón guardar
    tabla_chef que crea la tabla para mostrar los datos acerca de los platos
    mostrar_datos que trae los datos de la bd a la tabla
    editar_datos que permite editar los datos de la tabla
    actualizar_datos que se llama en el método anterior y sirve para actualizar los datos en la bd
    eliminar_datos que sirve para borrar los datos de la bd
    primera_interfaz que llevará a la primera interfaz
    """

    
    def __init__(self, root=None):
        super().__init__(root, width=480, height=320, bg='lightblue')
        self.root = root
        self.pack()
        self.root.title("Platos")
        self.id_plato=None
        self.chef=Chef()
        self.campos_plato()
        self.tabla_platos()
       
        
    def campos_plato(self):
     
     #Etiquetas   
     self.lbl_nombre_plato = tk.Label(self, text='Nombre plato:' , bg='lightblue')
     self.lbl_nombre_plato.config(font=('Arial', 12, 'bold'))
     self.lbl_nombre_plato.grid(row=0, column=0, padx=10, pady=10)

     self.lbl_chef_plato = tk.Label(self, text='Chef:' , bg='lightblue')
     self.lbl_chef_plato.config(font=('Arial', 12, 'bold'))
     self.lbl_chef_plato.grid(row=1, column=0, padx=10, pady=10)

     #Campos de texto
     self.var_nombre_plato = tk.StringVar()
     self.input_nombre_plato = tk.Entry(self, textvariable=self.var_nombre_plato)
     self.input_nombre_plato.config(width=50, font=('Arial', 12))
     self.input_nombre_plato.grid(row=0, column=1, padx=10, pady=10, columnspan=2)

     self.var_chef_plato = tk.StringVar()
     self.input_chef_plato = tk.Entry(self, textvariable=self.var_chef_plato)
     self.input_chef_plato.config(width=50, font=('Arial', 12))
     self.input_chef_plato.grid(row=1, column=1, padx=10, pady=10, columnspan=2)
     
     # botones
     self.btn_nuevo = tk.Button(self, text='Nuevo', command=self.habilitar_campos)
     self.btn_nuevo.config(width=20, font=('Arial', 12, 'bold'), bg='#FFFDC9')
     self.btn_nuevo.grid(row=2, column=0, padx=10, pady=10)

     self.btn_guardar = tk.Button(self, text='Guardar', command=self.guardar_datos_plato)
     self.btn_guardar.config(width=20, font=('Arial', 12, 'bold'), bg='#C7FFC6')
     self.btn_guardar.grid(row=2, column=1, padx=10, pady=10)

     self.btn_cancelar = tk.Button(self, text='Cancelar', command=self.desabilitar_campos)
     self.btn_cancelar.config(width=20, font=('Arial', 12, 'bold'), bg='#FFC6C6')
     self.btn_cancelar.grid(row=2, column=2, padx=10, pady=10)
     
     self.btn_volver = tk.Button(self, text='Volver', command=self.primera_interfaz)
     self.btn_volver.config(width=20, font=('Arial', 12, 'bold'), bg='#FFE1BD')
     self.btn_volver.grid(row=8, column=1, padx=10, pady=10)


    def habilitar_campos(self):
        self.var_nombre_plato.set('')
        self.var_chef_plato.set('')

        self.input_nombre_plato.config(state='normal')
        self.input_chef_plato.config(state='normal')

        self.btn_guardar.config(state='normal')
        self.btn_cancelar.config(state='normal')

    def desabilitar_campos(self):
        self.var_nombre_plato.set('')
        self.var_chef_plato.set('')

        self.input_nombre_plato.config(state='disabled')
        self.input_chef_plato.config(state='disabled')

        self.btn_guardar.config(state='disabled')
        self.btn_cancelar.config(state='disabled')

   
    def guardar_datos_plato(self):
    
     self.plato = Plato(
        self.var_nombre_plato.get(),
        self.var_chef_plato.get(),)
     
     self.plato.guardar()    
   
     self.tabla_platos()
     self.desabilitar_campos()
     
     
    def tabla_platos(self):
        
         self.tabla = ttk.Treeview(self,
         columns=('Nombre', 'Chef'))
         self.tabla.grid(row=4,column=0, columnspan=4,sticky='nse')
        
         self.scroll = ttk.Scrollbar(self,orient='vertical',command=self.tabla.yview)
         self.scroll.grid(row=4,column=0, columnspan=4,sticky='nse')
        
         self.tabla.configure(yscrollcommand=self.scroll.set)
         self.tabla.heading('#0',text='Id Plato')
         self.tabla.heading('#1',text='Nombre del plato')
         self.tabla.heading('#2',text='Identificacion del Chef')
        
        
         self.boton_editar = tk.Button(self,text="Editar",command=self.editar_datos)
         self.boton_editar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
         self.boton_editar.grid(row=5,column=0,padx=10,pady=10)
        
         self.boton_eliminar = tk.Button(self,text="Eliminar",command=self.eliminar_datos)
         self.boton_eliminar.config(width=20, font=('Arial',12,'bold'),bg='#c7c7e7')
         self.boton_eliminar.grid(row=5,column=1,padx=10,pady=10)
        
        
    def editar_datos(self):
        try:
            selected_item = self.tabla.selection()
            
            values = self.tabla.item(selected_item, 'values')
           
            if selected_item:
               values = self.tabla.item(selected_item, 'values')
               
               if values:
                    self.id_plato = values[0]
                    self.nombre_plato = values[1]
                    self.chef_plato = values[2]
                    self.habilitar_campos()
                    self.var_nombre_plato.set(self.id_plato)
                    self.var_chef_plato.set(self.chef_plato)
                    self.actualizar_datos()
                    
               else:
                messagebox.showerror("Error", "No se encontraron valores para la selección de la tabla.")
       
            else:
                messagebox.showerror("Error", "No se ha seleccionado ningún elemento en la tabla.")
       
           
        except Exception as error:
            print (error)  
            messagebox.showerror("Error", "Error al editar los datos")   
        
        
    def actualizar_datos(self):
        try:
             plato = Plato()
             plato.nombre_plato = self.var_nombre_plato.get()  # Obtiene el valor del campo de entrada
             plato.chef_plato = self.var_chef_plato.get()  # Obtiene el valor del campo de entrada
             plato.editar() # Llama método de la clase Plato que actualiza los datos 
            
        except Exception as error:
            print(error)
            messagebox.showerror("Error", "Error al actualizar los datos en la base de datos")    
     
        
    def eliminar_datos(self):
        try:
            self.id_plato = self.tabla.item(self.tabla.selection())['text']
            self.plato.eliminar(self.id_plato)
            self.tabla_platos()
            self.id_plato=None
            
        except Exception as error:
            print(error) 
            messagebox.showerror("Error", "Error al eliminar los datos")    
    
    def primera_interfaz (self):
        self.root.destroy()   #Cierra la ventana actual para llamar a la otra
        self.root = tk.Tk() 
        frame= Frame(self.root)
        frame.__init__