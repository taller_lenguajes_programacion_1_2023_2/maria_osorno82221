# *************** VARIABLES ******************

#funciones , type() tipo de variable , print() imprimir consola

# declaracion de variable 

# nombre = "hola mundo " #str
# print(type(nombre))

# #tipo de variable

# entero = 10 # int
# print(type(entero))

# flotante = 10.5 #float
# print(type(flotante)) evaluar tipo de variable

# booleano = True #bool
# print(type(booleano))

# ****************** estructura de datos ***********

# listas

# lista = [1,2,3,4,5]
# print(type(lista))

# » Las listas están asociadas, si cambio una lista2=lista, también se cambia la primera
# La solución es usar copy, ejemplo :: lista2=lista.copy


# lista2 = lista.copy()
# lista2.append(6)
# print(lista2)

# Diccionario
# diccionario = {
#     "nombre": "Ana",
#     "nombre": "andres",
#     "edad": 25}

# # Tupla No se le puede cambiar ningún valor (No mutable)
# tupla = (10, 20, 30)

# # Conjunto
# conjunto = {1, 2, 3}

# print(diccionario["nombre"])


# ********************** funciones *********

# # Uso de print para mostrar en pantalla
# print("Hola mundo")

# # Obtener longitud de una lista
# lista = [1, 2, 3]
# print(len(lista))

# # Conocer el tipo de una variable
# cadena= str(lista[1])
# print(type(cadena))
# entero = int(cadena)
# print(type(entero))


# ******************** LECTURA DE ARCHIVOS PLANOS ****************

#with open("E:/taller_programacion1/andres_callejas05082/src/trp1_sesiones/static/txt/sesion2.txt","r") as archivo:
# with open("./src/trp1_sesiones/static/txt/sesion2.txt","r") as archivo:
#     contenido = archivo.read()
# print(contenido)

# datos = "Esto es un ejemplo."
# with open("./src/trp1_sesiones/static/txt/sesion2_copia.txt", "w") as archivo:
#     archivo.write(datos)
# print(archivo)

# with open("E:/taller_programacion1/andres_callejas05082/src/trp1_sesiones/static/txt/sesion2.txt","r") as archivo:
# with open("./src/trp1_sesiones/static/txt/sesion2.txt","r") as archivo:
#     contenido = archivo.readlines()
# print(contenido[5])

#datos = "Esto es un ejemplo 2."
#with open("./src/trp1_sesiones/static/sesion2_copia.txt", "a") as archivo:
    #archivo.write("\n {}".format(datos))
