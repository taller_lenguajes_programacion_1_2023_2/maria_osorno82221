import sqlite3

import openpyxl



class Conexion:

    def __init__(self, db_file):

        self.conn = sqlite3.connect(db_file)

        self.cursor = self.conn.cursor()



    def ejecutar_sql(self, sql, parametros=None):

        if parametros:

            self.cursor.execute(sql, parametros)

        else:

            self.cursor.execute(sql)

        self.conn.commit()



    def cerrar_conexion(self):

        self.conn.close()



class Padre:

    def __init__(self, id, nombre, apellido, edad, direccion, telefono):

        self.id = id

        self.nombre = nombre

        self.apellido = apellido

        self.edad = edad

        self.direccion = direccion

        self.telefono = telefono



    # Implementa métodos CRUD para Padre



class Hijo(Padre):

    def __init__(self, id, nombre, apellido, edad, direccion, telefono,

                 escuela, grado, fecha_ingreso, fecha_fin, titulo, promedio):

        super().__init__(id, nombre, apellido, edad, direccion, telefono)

        self.escuela = escuela

        self.grado = grado

        self.fecha_ingreso = fecha_ingreso

        self.fecha_fin = fecha_fin

        self.titulo = titulo

        self.promedio = promedio



    # Implementa métodos CRUD adicionales para Hijo



def migrar_datos_excel_a_sqlite():

    # Abre el archivo Excel

    wb = openpyxl.load_workbook('datos.xlsx')

    hoja_padre = wb['Clase Padre']

    hoja_hijo = wb['Clase Hijo']



    # Crea la conexión a la base de datos SQLite

    conexion = Conexion('mi_base_de_datos.db')



    # Itera sobre las filas de las hojas de Excel y migra los datos a la base de datos SQLite

    for fila in hoja_padre.iter_rows(min_row=2, values_only=True):

        id, nombre, apellido, edad, direccion, telefono = fila

        padre = Padre(id, nombre, apellido, edad, direccion, telefono)

        # Llama a la función de creación en la clase Padre



    for fila in hoja_hijo.iter_rows(min_row=2, values_only=True):

        id, nombre, apellido, edad, direccion, telefono, escuela, grado, fecha_ingreso, fecha_fin, titulo, promedio = fila

        hijo = Hijo(id, nombre, apellido, edad, direccion, telefono,

                    escuela, grado, fecha_ingreso, fecha_fin, titulo, promedio)

        # Llama a la función de creación en la clase Hijo



    # Cierra la conexión a la base de datos

    conexion.cerrar_conexion()



if __name__ == "__main__":

    migrar_datos_excel_a_sqlite()