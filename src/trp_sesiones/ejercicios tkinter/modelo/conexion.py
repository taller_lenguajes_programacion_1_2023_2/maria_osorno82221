import sqlite3
from tkinter import messagebox

class Conexion:
    def __init__(self):
        self.ruta_db = './src/trp_sesiones/ejercicios tkinter/base de dato/peliculas.bd'
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()
        #self.crear_tabla()
        
    
    def crear_tabla(self):
        query= """
        CREATE TABLE peliculas (
            id_pelicula INTEGER PRIMARY KEY AUTOINCREMENT,
            nombre VARCHAR (100), 
            duracion VARCHAR (10),
            genero VARCHAR (100)
        )"""
        try:
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("INFO : Creación Tabla Exitosa","Se creó la tabla correctamente")
        except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Creación Tabla No Exitosa","No se pudo crear la tabla")

    def borrar_tabla(self):
        query= """
        DROP TABLE IF EXISTS peliculas"""
        try:
                self.cursor.execute(query)
                self.cerrar()
                messagebox.showinfo("INFO : Borrado Tabla Exitosa","Se eliminó la tabla correctamente")
        except Exception as error:
                print(error)
                messagebox.showerror("ERROR: Borrado Tabla No Exitosa","No se pudo eliminar la tabla")
   
    
    def cerrar(self):
        self.conexion.commit()
        self.conexion.close()
