#Taller Entregable1

#Ejercicio 8: Determinar si una variable es de tipo String o no
mxoa_cadena = "Este texto es un string"

if isinstance(mxoa_cadena, str):
    print("la variable es un string.")
else:
    print("la variable no es un string.")
    

#Ejercicio 18: Tomar una cadena y devolverla invertida
mxoa_cadena_invertida = mxoa_cadena[::-1]
print(mxoa_cadena_invertida)


#Ejercicio 28: Tomar dos listas y si ambas tienen algún elemento en común, devolver true
mxoa_lista1 = [1, 2, 3, 4, 5]
mxoa_lista2 = [4, 5, 6, 7, 8]

mxoa_elemento_comun = any(elemento in mxoa_lista1 for elemento in mxoa_lista2)
print(mxoa_elemento_comun)


#Ejercicio 38: Sumar todos los valores de un diccionario
mxoa_diccionario = {
    'elemento_A': 10,
    'elemento_B': 20,
    'elemento_C': 30,
    'elemento_D': 40
}
mxoa_suma = 0

for mxoa_valor in mxoa_diccionario.values():
    mxoa_suma =mxoa_suma + mxoa_valor

print("La suma de los valores del diccionario es:", mxoa_suma)


#Ejercicio 48: Combinar dos archivos CSV en uno solo
import csv

with open('archivo1.csv', 'r') as mxoa_archivo1, open('archivo2.csv', 'r') as mxoa_archivo2:
    mxoa_lector1 = csv.reader(mxoa_archivo1)
    mxoa_lector2 = csv.reader(mxoa_archivo2)
    
with open('archivo_salida.csv', 'w', newline='') as mxoa_archivo_salida:
    mxoa_escritor = csv.writer(mxoa_archivo_salida)
for fila in mxoa_lector1:
    mxoa_escritor.writerow(fila)
for fila in mxoa_lector2:
    mxoa_escritor.writerow(fila)
mxoa_archivo1.close()
mxoa_archivo2.close()
mxoa_archivo_salida.close()


#Ejercicio 58: Convertir un archivo CSV en JSON
import json

with open('mxoa_archivo1.csv', 'r') as archivo_csv:
   
    lector_csv = csv.DictReader(archivo_csv)
    mxoa_datos_json = []
    
    for fila in lector_csv:
        mxoa_datos_json.append(fila)


with open('mxoa_archivo_json.json', 'w') as archivo_json:
    json.dump( mxoa_datos_json, archivo_json, indent=4)



#Ejercicio 68: Simular un archivo excel alumnos.xlsx, ordenar los datos por el promedio del alumno
import pandas as pd
mxoa_archivo_excel = pd.read_excel('./src/trp_sesiones/1er_Entregable/Alumnos.xlsx', sheet_name='Alumnos')

mxoa_archivo_excel = mxoa_archivo_excel.sort_values(by='Promedio', ascending=False)
print(mxoa_archivo_excel)


#Ejercicio 78: Simula un archivo excel, mostrar un resumen estadístico de una columna numérica
mxoa_archivo_excel = pd.read_excel('./src/trp_sesiones/1er_Entregable/Alumnos.xlsx')
mxoa_columna_datos = mxoa_archivo_excel['Edad']
mxoa_resumen_estadistico = mxoa_columna_datos.describe()
print(mxoa_resumen_estadistico)


