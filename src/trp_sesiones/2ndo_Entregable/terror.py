from conexion import Conexion
from excel import Excel
import openpyxl


class Terror (Conexion): 
    """
     Esta clase 
    """
    def __init__(self, id = 0, fecha="",entradas=0,restriccion_edad="",valor_entrada="",descripcion=""):
        
        self.__xlsx = Excel("Terror_peliculas.xlsx")
        self.__df_ter = self.__xlsx.leer_xlsx("Terror")
        self.__lista_terror=self.__df_terror()

        
        self.__id_terror = id
        self.__fecha=fecha
        self.__entradas=entradas
        self.__restriccion_edad=restriccion_edad
        self.__valor_entrada=valor_entrada
        self.__descripcion=descripcion
        
        super().__init__()
          
        self.__crear_tabla_terror() 
        self.__insert_terror()
    
          
    def __crear_tabla_terror(self):
        if self.crear_tabla(nom_tabla="",nom_json="nom_per"):
          print(" ")
          
    def obtener_id(self,num=-1):
        if num < 0:
            return self.__lista_terror["numero"]
        else:
            self.__id= self.__lista_terror["numero"][num]
        return self.__id
      
      
    def __insert_terror(self):
        df = self.__df_per
        nom_colunas = "colums_terror"
        for index, row in df.iterrows():
            valores = '{}, "{}" , {} , "{}" , "{}" , "{}"'.format(row["id_terror"],row["fecha"],row["entradas"],row["restriccion_edad"],row["valor_entrada"],row["descripcion"])
            if self.insert_datos(nom_tabla="Terror",nom_columnas=nom_colunas,valores=valores):
                print("Inserto los valores: ",valores)
            else:
                print("No inserto los valores : ",valores)


       
    

    
        
        
          
          
    @property
    def _id(self):
      return self.__id

    @_id.setter
    def _id(self,id=""):
        self.__id = id
              
    @property
    def _fecha(self):
      return self.__fecha

    @_fecha.setter
    def _fecha(self, fecha=""):
        self.__fecha = fecha
        
    @property
    def _entradas(self):
      return self.__entradas

    @_entradas.setter
    def _entradas(self, entradas=0):
        self.__entradas = entradas
    
    
    @property
    def _valor_entrada(self):
      return self.__valor_entrada

    @_fecha.setter
    def _valor_entrada(self, valor_entrada=""):
        self.__valor_entrada = valor_entrada
        
        
    @property
    def _restriccion_edad(self):
      return self.__restriccion_edad

    @_restriccion_edad.setter
    def _restriccion_edad(self, restriccion_edad=""):
        self.__restriccion_edad = restriccion_edad
        
        
    @property
    def _descripcion(self):
      return self.__descripcion

    @_descripcion.setter
    def _descripcion(self, descripcion=""):
        self.__descripcion = descripcion
        
    def __df_terror(self):
      df = self.__df_ter
      lista_ter = df.to_dict()
      return lista_ter
    
    def __str__(self):
     return f"{self.__id_terror} {self.__fecha} {self.__entradas} {self.__restriccion_edad} {self.__descripcion}"
 
 


        

      
        
         
class Pelicula(Terror):  
    def __init__(self,id=0, nombre=" ",duracion=0,	productor=" ",	origen=" ",	actores=" "):
       
        super().__init__(self,id = 0, fecha="",entradas="",restriccion_edad="",valor_entrada="",descripcion="")
        
        self.__id = id
        self.__nombre=nombre
        self.__duracion=duracion
        self.__productor=productor
        self.__origen=origen
        self.__actores=actores
        
        @property
        def _id(self):
          return self.__id

        @_id.setter
        def _id(self, id=0):
          self.__id = id  
    
    
    @property
    def _nombre(self):
      return self.__nombre

    @_nombre.setter
    def _id(self, nombre=""):
        self.__nombre = nombre
        
    @property
    def _duracion(self):
      return self.__duracion

    @_duracion.setter
    def _duracion(self, duracion=0):
        self.__duracion = duracion
    
    
    @property
    def _productor(self):
      return self.__productor

    @_productor.setter
    def _valor_entrada(self, productor=""):
        self.__productor = productor
        
        
    @property
    def _origen(self):
      return self.__origen

    @_origen.setter
    def _origen(self, origen=""):
        self.__origen = origen
        
        
    @property
    def _actores(self):
      return self.__actores

    @_actores.setter
    def _actores(self, actores=""):
        self.__actores = actores
        
        
   #---------------
   
   
    @property
    def _id(self):
        return self.__id
      
    @_id.setter
    def _id(self, id=0): 
      self.__id = id 
   
    @property
    def _fecha(self):
      return self.__fecha

    @_fecha.setter
    def _id(self, fecha=""):
        self.__fecha = fecha
        
    @property
    def _entradas(self):
      return self.__entradas

    @_fecha.setter
    def _entradas(self, entradas=""):
        self.__entradas = entradas
    
    
    @property
    def _valor_entrada(self):
      return self.__valor_entrada

    @_fecha.setter
    def _valor_entrada(self, valor_entrada=""):
        self.__valor_entrada = valor_entrada
        
        
    @property
    def _restriccion_edad(self):
      return self.__restriccion_edad

    @_restriccion_edad.setter
    def _restriccion_edad(self, restriccion_edad=""):
        self.__restriccion_edad = restriccion_edad
        
        
    @property
    def _descripcion(self):
      return self.__descripcion

    @_descripcion.setter
    def _descripcion(self, descripcion=""):
        self.__descripcion = descripcion     
        
        
    




 #-----------------------------------------------------    
        
    def migrar_datos_excel_a_sqlite():

    # Abre el archivo Excel
 
    wb = openpyxl.load_workbook('./src/trp_sesiones/2ndo_Entregable/static_/Terror_peliculas.xlsx')

    hoja_padre = wb['Terror']

    hoja_hijo = wb['Pelicula']



    # Crea la conexión a la base de datos SQLite

    conexion = Conexion('base_datos_pelicula.sqlite')



    # Itera sobre las filas de las hojas de Excel y migra los datos a la base de datos SQLite

    for fila in hoja_padre.iter_rows(min_row=2, values_only=True):

        id, fecha,entradas,restriccion_edad,valor_entrada,descripcion = fila

        terror = Terror(id, fecha , restriccion_edad, valor_entrada, descripcion)

        # Llama a la función de creación en la clase Padre



    for fila in hoja_hijo.iter_rows(min_row=2, values_only=True):

        id, nombre,duracion,	productor,	origen,	actores = fila

        pelicula = Pelicula(id, nombre, duracion, productor, origen, actores)

        # Llama a la función de creación en la clase Hijo



    # Cierra la conexión a la base de datos

    conexion.cerrar_conexion()


   