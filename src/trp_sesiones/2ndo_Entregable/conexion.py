import json
import sqlite3
import openpyxl


class Conexion:
    
    def __init__(self, db_file):

        self.conn = sqlite3.connect(db_file)

        self.cursor = self.conn.cursor()



    def ejecutar_sql(self, sql, parametros=None):

        if parametros:

            self.cursor.execute(sql, parametros)

        else:

            self.cursor.execute(sql)

        self.conn.commit()



    def cerrar_conexion(self):

        self.conn.close()
    
    
    
    
    
    #------------------------------------------------------------------------------------------------
    
    def __init__(self) :
        __nom_db = "db_peliculas.sqlite"
        self.__querys = self.obtener_json()
        self.__conx = sqlite3.connect("./src/trp_sesiones/2ndo_Entregable/static_/db/{}".format(__nom_db))
        self.__cursor = self.__conx.cursor()
        
        
    def obtener_json(self):
        ruta = "./src/trp_sesiones/2ndo_Entrgable/static_/querys.json"
        querys = {}
        with open(ruta,'r') as file_json:
            querys = json.load(file_json)
        return querys
    
    def crear_tabla(self,nom_tabla = "", nom_json = ""):
        if nom_tabla != "" and nom_json != "":
            columns = self.__querys[nom_json]
            query = self.__querys["crear_tabla"].format(nom_tabla,columns)
            self.__cursor.execute(query)
            self.__conx.commit()
            return True
        return False
    
    def insert_datos(self,nom_tabla="", nom_columnas="", valores=""):
        if nom_tabla!=""and len(nom_columnas) >0 and len(valores)>0:
            query = self.__querys["insertar_datos"]
            nom_columnas = self.__querys[nom_columnas]
            query = query.format(nom_tabla,nom_columnas,valores)
            self.__cursor.execute(query)
            self.__conx.commit()

            return True
        else:
            return False
    
    def update_datos(self,nom_tabla ="", columns_valores="",id = ""):
        if nom_tabla != "" and id != "":
            query = self.__querys["update_datos"]
            query = query.format(nom_tabla,columns_valores,id)
            self.__cursor.execute(query)
            self.__conx.commit()
            return True
        else:
            return False
    
    def delete_datos(self,nom_tabla ="", id = ""):
        if nom_tabla != "" and id != "":
            query = self.__querys["delete_datos"] 
            query = query.format(nom_tabla,id)
            self.__cursor.execute(query)
            self.__conx.commit()
            return True
        else:
            return False
