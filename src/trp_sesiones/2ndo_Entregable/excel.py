import pandas as pd
import openpyxl

class Excel:
    def __init__(self,nombre_xlsx= "") -> None:
        self.___nombre_xlsx = nombre_xlsx
        self.__ruta_xlsx = "./src/trp_sesiones/2ndo_Entregable/static_/{}".format(self.___nombre_xlsx)
        
    
    def leer_xlsx(self, nom_hoja=""):
        if nom_hoja == "":
            df = pd.read_excel(self.__ruta_xlsx)
        else:
            df = pd.read_excel(self.__ruta_xlsx,sheet_name=nom_hoja)
        return df
