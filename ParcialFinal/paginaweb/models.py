from django.db import models

# Create your models here.

from django.db import models

# Create your models here.


class AuditoriaFecha(models.Model):
    f_creacion = models.DateTimeField(auto_now_add=True)
    f_actualizacion = models.DateTimeField(auto_now=True)
    class Meta:
        abstract = True
    
class Persona(AuditoriaFecha):
    usuario = models.CharField(max_length=255)
    clave = models.CharField(max_length=255)
    email=models.CharField(max_length=255)
    fecha_nacimiento = models.DateField()
    
    
    def __str__(self) :
        return "Persona:  {} ".format(self.usuario)
    

class Producto(AuditoriaFecha):
    nombre = models.CharField(max_length=255)
    descripcion=models.CharField(max_length=255)
    ingredientes=models.CharField(max_length=255)
    cantidad =models.IntegerField()
    valor = models.FloatField()
    imagen= models.ImageField(upload_to='imagenes/',null=True)
    
    
    def __str__(self) :
        return "Producto:  {}".format(self.nombre)
    
    def delete(self, using=None, keep_parents=False):
        self.imagen.storage.delete(self.imagen.name)
        super().delete()



