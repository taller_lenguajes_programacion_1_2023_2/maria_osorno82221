from django import forms
from .models import Persona
from django.contrib.auth.forms import AuthenticationForm

class LoginForm(forms.Form):
    usuario = forms.CharField(required=True,widget=forms.TextInput())
    clave = forms.CharField(required=True,widget=forms.PasswordInput())
    
    class Meta:
        model = Persona
        
        
        
class PersonaForm(forms.ModelForm):
    
    def clean_usuario(self):
        usuario = self.cleaned_data['usuario']
        if Persona.objects.filter(usuario=usuario).exists():
            raise forms.ValidationError("Este nombre de usuario ya está en uso. Por favor, elige otro.")
        return usuario
    
    
    class Meta:
        model = Persona
        fields = '__all__'        
        
        