from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Persona, Producto
from .forms import LoginForm, PersonaForm
from django.shortcuts import render, get_object_or_404

# Create your views here.

def inicio(request):
       return render(request, 'paginas/inicio.html')



def login(request):
    mensaje = {"estado": "", "mensaje": "Bienvenido"}
    usuariodb = None

    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        usuario = request.POST.get('usuario', '')
        clave = request.POST.get('clave', '')

        usuariodb = Persona.objects.filter(usuario=usuario, clave=clave)
        if len(usuariodb) > 0:
            return redirect("inicio")
        else:
            messages.error(request, 'Usuario o clave incorrecto. Por favor, inténtalo de nuevo.')
            mensaje['form_login'] = form
            return render(request, 'paginas/login.html', {'formulario': form, 'mensaje_error': messages.get_messages(request)})
    else:
        form = LoginForm(data=request.GET)
        mensaje['form_login'] = form

    return render(request, 'paginas/login.html', {'formulario': form, 'mensaje_error': None})



def registro(request):
       formulario=PersonaForm(request.POST or None)
       if request.method == 'POST':
        if formulario.is_valid():
              formulario.save()
              messages.success(request, 'Se registró con éxito')
              return redirect('login')
       
       return render(request, 'paginas/registro.html', {'formulario':formulario})



def productos(request):
    producto=Producto.objects.all()
    #print(producto) 
    return render(request, 'paginas/productos.html',{'producto': producto})



def detalle(request, producto_id):
    #producto = Producto.objects.get(id=producto_id)
    producto = get_object_or_404(Producto, id=producto_id)
    return render(request, 'paginas/detalle.html', {'producto': producto})


       
       